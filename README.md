# TIAGo - Single Arm with PAL Gripper (MJCF)

## Overview

Based on the model for [ANYmal](https://github.com/llach/mujoco_menagerie/blob/main/anybotics_anymal_b/README.md).

This package contains a simplified robot description (MJCF) of the [TIAGo
robot]() developed by
[PAL Robotics](). It is derived from ???.

<p float="left">
  <img src="tiago.png" width="400">
</p>

## URDF → MJCF derivation steps

1. Dumped URDF file from TIAGo sim `rosparam get -p /robot_description > tiago.urdf`
2. Added `<mujoco> <compiler discardvisual="false" meshdir="meshes/" balanceinertia="true"/> </mujoco>` to the URDF's `<robot>` tag.
3. Converted `.dae` files to `.stl` files using [Blender](https://www.blender.org/).
4. Loaded the URDF into MuJoCo and saved a corresponding MJCF.

## License

This model is released under a ?LICENSE?.

## Publications

